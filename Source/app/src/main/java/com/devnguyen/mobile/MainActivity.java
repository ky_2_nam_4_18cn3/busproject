package com.devnguyen.mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createListName();
    }

//    Create lít name
    private void createListName(){
        ListView lstViewName;
        EditText inputName;
        Button btnAdd;
        ArrayList<String> lstName;

        lstViewName = (ListView) findViewById(R.id.lst_name);
        inputName = (EditText) findViewById(R.id.input_name);
        btnAdd = (Button) findViewById(R.id.btn_add);


        lstName = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            lstName.add("Name " + i);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity.this,
                android.R.layout.simple_list_item_1,
                lstName);
        lstViewName.setAdapter(arrayAdapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tmpName = inputName.getText().toString();

                lstName.add(tmpName);
                arrayAdapter.notifyDataSetChanged();
                Toast.makeText(
                        MainActivity.this,
                        "Thêm mới thành công",
                        Toast.LENGTH_SHORT
                ).show();
                inputName.getText().clear();
            }
        });
    }
}